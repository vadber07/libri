class Formation {
   /**
   * nomFormation
   * type
   * description
   * prix
   */
   constructor(id, title, description, prix, type) {
      this.id = id;
      this.title = title;
      this.description = description;
      this.prix = prix;
      this.type = type;
   }
}

const formations = [
   new Formation(0, "Nodejs formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "150$", "Nodejs"),
   new Formation(1, "MongoDB formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "100$", "MongoDB"),
   new Formation(2, "Nodejs formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "175$", "Nodejs"),
   new Formation(3, "MongoDB formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "420$", "MongoDB"),
   new Formation(4, "Nodejs formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "99$", "Nodejs"),
   new Formation(5, "MongoDB formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "125$", "MongoDB"),
   new Formation(6, "Nodejs formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "155$", "Nodejs"),
   new Formation(7, "MongoDB formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "350$", "MongoDB"),
   new Formation(8, "Nodejs formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "350$", "Nodejs"),
   new Formation(9, "MongoDB formation", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget ornare nisl. Maecenas eget augue sodales, pulvinar elit sit amet, porta erat. Fusce egestas congue felis et pulvinar. Cras dictum sed orci at venenatis. Curabitur id nulla non leo pulvinar imperdiet vel vulputate libero. Cras in velit et velit rhoncus luctus. Nunc tincidunt tellus ut urna fringilla gravida.", "150$", "MongoDB")
]

exports.allFormations = formations;

exports.getFormationByType = function get_by_types(type) {
   var formationstype = []
   for (let i = 0; i < formations.lengh; i++) {
      if (formations[i].type == type) {
         formationstype.push(formations[i]);
      }
   }
   return formations[type];
};


exports.getFormationById = function (id) {
   return formations[id];
};