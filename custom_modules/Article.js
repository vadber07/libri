class Article {
   /*
      * id
      * titre
      * auteur
      * p
      */
   constructor(id, title, author, description) {
      this.id = id;
      this.title = title;
      this.description = description;
      this.author = author;
   }
}

const articles = [
   new Article(0, "Lorem ipsum", "Bob Ross", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(1, "Lorem ipsum", "Al Capone", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(2, "Lorem ipsum", "Summer Smith", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(3, "Lorem ipsum", "Onxy Lavita", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(4, "Lorem ipsum", "Jean adit", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(5, "Lorem ipsum", "Sam i-am", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(6, "Lorem ipsum", "Jean-Claude Van dam", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(7, "Lorem ipsum", "Ty Fisher", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(8, "Lorem ipsum", "Hatsune Miku", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat."),
   new Article(9, "Lorem ipsum", "Mario Brothers", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet ligula lorem, a malesuada enim congue sit amet. Sed sed cursus dui, ut malesuada leo. Cras egestas neque sed euismod sagittis. Aliquam sodales maximus nunc at consequat. Nullam imperdiet scelerisque placerat.")
]

exports.allArticles = articles;


exports.getArticleById = function getArticles(id) {
   return articles[id];
};