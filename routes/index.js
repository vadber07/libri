var express = require('express');
var Router = express.Router();
let formation = require("../custom_modules/Form.js");
let article = require("../custom_modules/Article.js");

/* GET home page. */
Router.get('/', function (req, res) {
  res.render('index', { title: 'Accueille' });
});

/* GET des formation page. */
Router.get('/formations', function (req, res, next) {
  res.render('formations', {
    nom: "Nos formations",
    formation: formation.allFormations
  });
});

/* GET formation page. */
Router.get('/formation/:id', function (req, res, next) {
  res.render('formation', {
    formation: formation.getFormationById(req.params.id)
  });
});

/* GET farmation par type page. */
Router.get('/formations/type/:type', function (req, res, next) {
  res.render('formation', {
    formation: formation.getFormationByType(req.params.type)
  });
});

/* GET article page. */
Router.get('/article/:id', function (req, res, next) {
  res.render('article', {
    article: article.getArticleById(req.params.id)
  });
});

/* GET contact page. */
Router.get('/contact', function (rep, res, next) {
  res.render('contact', {
    title: "nous contacter"
  });
});

/* GET blog page. */
Router.get('/blog', function (rep, res, next) {
  res.render('blog', {
    title: 'blog',
    article: article.allArticles
  });
});

module.exports = Router;